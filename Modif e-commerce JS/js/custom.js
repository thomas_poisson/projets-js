

//création variable changement logo header

/* Théo : possibilité de remplacer "div.header-logo img" par ".header-logo" */
    let headlogo = document.querySelector("div.header-logo img");
    headlogo.src = "./img/logo_default.png";

    console.log(headlogo)


    //création var pour enlever la sidebar

    /* Théo : document.querySelector('.main > .container > .row > .col-lg-3').style.display="none"; */
    document.querySelector('div.main > div.container > div.row > div.col-lg-3').style.display="none";


    //création var changement images produits

    /* Pour une mailleur visibilité pense à "mainImg" "secondeImg" "thirdImg" au lieux de "mainimg" "secondimg" "thirdimg"
    et toujours la posibilité de retirer les "div" avant les .class */
    let mainimg = document.querySelector("div.owl-carousel div:nth-child(1n) img")
    let secondimg = document.querySelector("div.owl-carousel div:nth-child(2) img")
    let thirdimg = document.querySelector("div.owl-carousel div:nth-child(3) img")

    //changement d'images carousel
    mainimg.src = "./img/sac_blanc.jpg";
    secondimg.src = "./img/sac_gris.jpg"; 
    thirdimg.src = "./img/sac_noir.jpg"; 

    //sélection div.col-lg-9/ puis rename en col-lg-12
    document.querySelector('div.main > div.container > div.row > div.col-lg-9').className="col-lg-12";


    let achat = document.querySelector('div.main > div.container > div.row > div.col-lg-12 > div.row > .col-lg-6:nth-child(2) div.summary');

    //sélection div formacart et prometa
    let formcart = document.querySelector('form.cart');
    let prometa = document.querySelector('div.product-meta');

    //Inversement de l'ordre
    achat.insertBefore(prometa, formcart);

    //css de formcart 
    formcart.style.display="flex";
    formcart.style.marginTop="50px";
    formcart.style.marginRight="66px";
    formcart.style.flexDirection="column";
    formcart.style.alignItems="center";



    //sélection div inventaire et css
    let quant = document.querySelector("div.quantity.quantity-lg");

    quant.style.marginLeft= "13px";

    /* Pense au nom de tes classes si un développeur reprend ton code dans quelque semaine comme nommer dans le brief.
    img / img1 / img2 / img3 / img4 tu peux les remplacer par imgExplication */
    
    let img = document.querySelector('.masonry-loader > .row > .product:nth-child(1) > span > a:nth-child(2) > span > img');
    img.src ="./img/appphoto.jpg";

    let img2 = document.querySelector('.masonry-loader > .row > .product:nth-child(2) > span > a:nth-child(2) > span > img');
    img2.src ="./img/sac-golf.jpg";

    let img3 = document.querySelector('.masonry-loader > .row > .product:nth-child(3) > span > a:nth-child(2) > span > img');
    img3.src ="./img/sport.jpg";

    let img4 = document.querySelector('.masonry-loader > .row > .product:nth-child(4) > span > a:nth-child(2) > span > img');
    img4.src ="./img/luxury_bag.jpg";

    document.querySelector('form#newsletterForm > div.input-group > span > button.btn').style.borderRadius="0px";
